import { container } from '../inversify.config';
import { componentSymbols } from './IoC/Symbols';
import { IInitApp } from './interfaces/interfaces';
import './main.scss';

window.onload = () => {
    const initApp = container.get<IInitApp>(componentSymbols.initApp);
    initApp.init();
};
