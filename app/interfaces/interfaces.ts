import * as go from 'gojs';

export interface IInitApp {
    init: () => void;
}

export interface ILocalStorageManager {
    getInitalDiagramModels: (key: string) => string[];
    setToLocalStorage: (key: string, value: string[]) => void;
}

export interface IDiagramNodeTemplateProvider {
    provideTemplates: () => NodeTemplate[];
}

export interface IPaletteNodeTemplateProvider {
    provideTemplate: () => NodeTemplate;
}

export interface IButtonsForDiagram {
    registerClickListeners: () => void;
}

export interface IButtonsForModels {
    generateButtons: (keys: string[], activeModelKey: string, onClick: (id: string) => void) => void;
}

export interface IButtonsForLayout {
    registerEventListeners: (callbackAutoMode: Function, callbackManualMode: Function, callbackOnKeyDown: Function) => void;
}

export interface IDiagramModels {
    init: () => void;
    addModel: () => void;
    saveModels: () => void;
    getModel: (key: string) => go.Model;
}

export interface IDiagramAccessor {
    setUpOnDiagram: (diagram: go.Diagram) => void;
    getDiagram: () => go.Diagram;
    setNewModel: (newModel?: go.Model) => void;
}

export interface IDiagramLayoutManager {
    init: () => void;
}

interface NodeTemplate {
    category: string,
    template: go.Node
}