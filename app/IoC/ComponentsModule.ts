import { ContainerModule, interfaces, decorate, injectable } from 'inversify';
import { componentSymbols } from './Symbols';
import * as go from 'gojs';
import { Diagram } from '../components/Diagram';
import { Palette } from '../components/Palette';
import { IDiagramNodeTemplateProvider, IPaletteNodeTemplateProvider, IDiagramModels, IButtonsForDiagram, IDiagramAccessor, IButtonsForModels, IButtonsForLayout, IDiagramLayoutManager, ILocalStorageManager } from '../interfaces/interfaces';
import { DiagramNodeTemplateProvider } from '../components/templateProviders/DiagramNodeTemplateProvider';
import { PaletteNodeTemplateProvider } from '../components/templateProviders/PaletteNodeTemplateProvider';
import { PaletteNodeTemplateProvider2 } from '../components/templateProviders/PaletteNodeTemplateProvider2';
import { ButtonsForDiagram } from '../components/ButtonsForDiagram';
import { DiagramModels } from '../components/DiagramModels';
import { DiagramAccessor } from '../components/DiagramAccessor';
import { ButtonsForModels } from '../components/ButtonsForModels';
import { InitApp } from '../components/InitApp';
import { ButtonsForLayout } from '../components/ButtonsForLayout';
import { DiagramLayoutManager } from '../components/DiagramLayoutManager';
import { CustomLayout } from '../components/CustomLayout';
import { LocalStorageManager } from '../components/LocalStorageManager';

const componentsModule = new ContainerModule((bind: interfaces.Bind) => {
    decorate(injectable(), go.Diagram);
    decorate(injectable(), go.Palette);

    bind(componentSymbols.initApp).to(InitApp);
    bind<ILocalStorageManager>(componentSymbols.localStorageManager).to(LocalStorageManager);

    bind(componentSymbols.diagram).to(Diagram).inSingletonScope();
    bind<interfaces.Factory<Diagram>>(componentSymbols.diagramFactory)
        .toAutoFactory(componentSymbols.diagram);

    bind<IDiagramNodeTemplateProvider>(componentSymbols.diagramNodeTemplateProvider).to(DiagramNodeTemplateProvider);

    bind<IDiagramModels>(componentSymbols.diagramModels).to(DiagramModels).inSingletonScope();
    bind<IDiagramAccessor>(componentSymbols.diagramAccessor).to(DiagramAccessor).inSingletonScope();
    bind<IDiagramLayoutManager>(componentSymbols.diagramLayoutManager).to(DiagramLayoutManager);

    bind(componentSymbols.palette).to(Palette);
    bind<interfaces.Factory<Palette>>(componentSymbols.paletteFactory)
        .toAutoFactory(componentSymbols.palette);

    bind<IPaletteNodeTemplateProvider>(componentSymbols.paletteNodeTemplateProvider).to(PaletteNodeTemplateProvider);
    bind<IPaletteNodeTemplateProvider>(componentSymbols.paletteNodeTemplateProvider).to(PaletteNodeTemplateProvider2);

    bind<IButtonsForDiagram>(componentSymbols.buttonsForDiagram).to(ButtonsForDiagram);
    bind<IButtonsForModels>(componentSymbols.buttonsForModels).to(ButtonsForModels);
    bind<IButtonsForLayout>(componentSymbols.buttonsForLayout).to(ButtonsForLayout);

    bind(componentSymbols.customLayout).to(CustomLayout);
});

export default componentsModule;
