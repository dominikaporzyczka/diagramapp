import { ContainerModule, interfaces } from 'inversify';
import { constantsSymbols } from './Symbols';

const constantsModule = new ContainerModule((bind: interfaces.Bind) => {

    bind(constantsSymbols.canvasSelector).toConstantValue('.canvas');
    bind(constantsSymbols.paletteSelector).toConstantValue('.palette');

});

export default constantsModule;
