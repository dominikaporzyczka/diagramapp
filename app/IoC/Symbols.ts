export const constantsSymbols: any = {
    canvasSelector: Symbol('canvasSelector'),
    paletteSelector: Symbol('paletteSelector')
};

export const componentSymbols: any = {
    initApp: Symbol('initApp'),
    localStorageManager: Symbol('localStorageManager'),

    diagram: Symbol('diagram'),
    diagramFactory: Symbol('diagramFactory'),
    diagramNodeTemplateProvider: Symbol('diagramNodeTemplateProvider'),
    diagramModels: Symbol('diagramModels'),
    diagramAccessor: Symbol('diagramAccessor'),
    diagramLayoutManager: Symbol('diagramLayoutManager'),

    palette: Symbol('palette'),
    paletteFactory: Symbol('paletteFactory'),
    paletteNodeTemplateProvider: Symbol('paletteNodeTemplateProvider'),

    buttonsForDiagram: Symbol('buttonsForDiagram'),
    buttonsForModels: Symbol('buttonsForModels'),
    buttonsForLayout: Symbol('buttonsForLayout'),
    customLayout: Symbol('customLayout')
};