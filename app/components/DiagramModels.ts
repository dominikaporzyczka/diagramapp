import * as go from 'gojs';
import { injectable, inject } from 'inversify';
import { IDiagramModels, IButtonsForModels, IDiagramAccessor, ILocalStorageManager } from '../interfaces/interfaces';
import { componentSymbols } from '../IoC/Symbols';

interface IModelObj {
    model: string;
    isActive: boolean;
}

@injectable()
export class DiagramModels implements IDiagramModels {
    private diagramModels: Map<string, IModelObj> = new Map();
    private currentlyActiveKey: string;

    constructor(@inject(componentSymbols.diagramAccessor) private diagramAccessor: IDiagramAccessor,
        @inject(componentSymbols.buttonsForModels) private buttonsForModels: IButtonsForModels,
        @inject(componentSymbols.localStorageManager) private localStorageManager: ILocalStorageManager) {
    }

    init() {
        const models = this.localStorageManager.getInitalDiagramModels('models');
        const key = models ? (models.length - 1).toString() : '0';

        !!models ? this.createInitialDiagramModelsMap(models) : this.diagramModels.set(key, { model: '', isActive: true });

        this.updateDiagram(key, this.getInitialModel());
    }

    addModel() {
        const isModelExist = this.diagramModels.has(this.currentlyActiveKey)
        const keyNewModel = this.getModelsKeys().length.toString()
        const keyModel: string = isModelExist ? this.currentlyActiveKey : keyNewModel;

        this.saveCurrentModelToMap(keyModel);
        this.createNewEmptyModel();
    }

    saveModels() {
        this.saveCurrentModelToMap(this.currentlyActiveKey);
        this.localStorageManager.setToLocalStorage('models', this.getAllModels(this.diagramModels));
    }

    getModel(key: string) {
        const modelObj: IModelObj = this.diagramModels.get(key);
        const modelJson = modelObj.model;

        if (modelJson) {
            const model: go.Model = go.Model.fromJson(modelJson);
            return model;
        }
    }

    private updateDiagram(key: string, model? : go.Model) {
        this.updateCurrentlyActiveModel(key, model);
        this.buttonsForModels.generateButtons(this.getModelsKeys(), key, this.displayNewModel.bind(this));
    }

    private createNewEmptyModel() {
        const keyNewEmptyModel: string = (this.getModelsKeys().length).toString();

        this.diagramModels.set(keyNewEmptyModel, { model: '', isActive: false });
        this.updateDiagram(keyNewEmptyModel);
    }

    private displayNewModel(key: string) {
        const nextModel = this.getModel(key);

        this.saveCurrentModelToMap(this.currentlyActiveKey);
        this.updateCurrentlyActiveModel(key, nextModel);
    }

    private updateCurrentlyActiveModel(key: string, model: go.Model) {
        this.currentlyActiveKey = key;
        this.setModelToActive(key);
        this.diagramAccessor.setNewModel(model);
    }

    private saveCurrentModelToMap(key: string) {
        const currentModel: string = this.diagramAccessor.getDiagram().model.toJson();
        this.diagramModels.set(key, { model: currentModel, isActive: false })
    }

    private getModelsKeys() {
        return [...this.diagramModels.keys()];
    }

    private getNumberOfModels(): number {
        return this.diagramModels.size;
    }

    private getAllModels(models: Map<string, IModelObj>): string[] {
        const setOfModels = [];
        models.forEach(item => {
            setOfModels.push(item.model);
        });

        return setOfModels;
    }

    private setModelToActive(key: string) {
        const modelObj: IModelObj = this.diagramModels.get(key);

        this.resetActiveFlags();
        this.diagramModels.set(key, { ...modelObj, isActive: true });
    }

    private resetActiveFlags() {
        this.diagramModels.forEach(modelObj => {
            modelObj.isActive = false;
        });
    }

    private getInitialModel = () => {
        const numberOfSavedModels = this.getNumberOfModels();
        const keyOfModel = (numberOfSavedModels - 1).toString();

        if (numberOfSavedModels) {
            const lastSavedModel = this.getModel(keyOfModel);

            return lastSavedModel;
        }
    }

    private createInitialDiagramModelsMap(models: string[]) {
        this.diagramModels = models.reduce((map: Map<string, IModelObj>, currentValue: string, i: number) => {
            map.set(i.toString(), {model: currentValue, isActive: false});
            return map;
        }, new Map<string, IModelObj>());
    }
}
