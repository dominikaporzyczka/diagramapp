import * as go from 'gojs';
import { injectable } from 'inversify';
import { IDiagramAccessor } from '../interfaces/interfaces';

@injectable()
export class DiagramAccessor implements IDiagramAccessor {
    private diagram: go.Diagram;

    setUpOnDiagram(diagram: go.Diagram) {
        this.diagram = diagram;
    }

    getDiagram() {
        return this.diagram;
    }

    setNewModel(newModel?: go.Model) {
        if (newModel) {
            this.diagram.model = newModel;
            return;
        }

        this.diagram.model = new go.GraphLinksModel([], []);
    }
}