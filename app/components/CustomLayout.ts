import * as go from 'gojs';
import { injectable, decorate } from 'inversify';

decorate(injectable(), go.Layout);

@injectable()
export class CustomLayout extends go.GridLayout {
    constructor() {
        super();
    }
}
