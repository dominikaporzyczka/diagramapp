import * as go from 'gojs';
import { injectable, inject } from 'inversify';
import { constantsSymbols, componentSymbols } from '../IoC/Symbols';
import { IDiagramNodeTemplateProvider, IDiagramAccessor } from '../interfaces/interfaces';

@injectable()
export class Diagram extends go.Diagram {
    constructor( @inject(constantsSymbols.canvasSelector) canvasSelector: string,
        @inject(componentSymbols.diagramAccessor) diagramAccessor: IDiagramAccessor,
        @inject(componentSymbols.diagramNodeTemplateProvider) nodeTemplateProvider: IDiagramNodeTemplateProvider) {
        super(document.querySelector(canvasSelector) as HTMLDivElement);

        diagramAccessor.setUpOnDiagram(this);

        this.allowDrop = true;

        const templates = nodeTemplateProvider.provideTemplates();
        templates.forEach(item => {
            this.nodeTemplateMap.add(item.category, item.template);
        });
    }
}
