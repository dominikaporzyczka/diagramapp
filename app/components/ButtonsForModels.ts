import { injectable } from "inversify";
import { IButtonsForModels } from "../interfaces/interfaces";

@injectable()
export class ButtonsForModels implements IButtonsForModels {
    private btnActiveClassName: string = 'active';

    generateButtons(keys: string[], activeModelKey: string, onClick: (key: string) => void) {
        const btnsContainer: HTMLElement = document.querySelector('.btns-models-container');
        btnsContainer.innerHTML = '';

        keys.forEach((key, index) => {
            const btnEl: HTMLButtonElement = document.createElement('button');

            btnEl.classList.add('btn');
            btnEl.innerHTML = `${index + 1}`;
            btnsContainer.appendChild(btnEl);

            this.registerClickListener(btnEl, key, onClick);

            if (key === activeModelKey) {
                this.resetActiveClassNameFromBtns();
                this.setActiveClassName(btnEl);
            }
        });
    }

    private registerClickListener(btn: HTMLButtonElement, key: string, callback: (key: string) => void) {

        btn.addEventListener('click', () => {
            this.resetActiveClassNameFromBtns();
            this.setActiveClassName(btn);
            callback(key);
        });
    }

    private resetActiveClassNameFromBtns() {
        const btn = document.querySelector(`.btns-models-container .${this.btnActiveClassName}`);
        if (btn) {
            btn.classList.remove(this.btnActiveClassName);
        }
    }

    private setActiveClassName(el: HTMLElement) {
        el.classList.add(this.btnActiveClassName);
    }
}