import * as go from 'gojs';
import { IDiagramLayoutManager, IDiagramAccessor, IButtonsForLayout } from "../interfaces/interfaces";
import { inject, injectable } from "inversify";
import { componentSymbols } from "../IoC/Symbols";

@injectable()
export class DiagramLayoutManager implements IDiagramLayoutManager {
    private diagram: go.Diagram;

    constructor(@inject(componentSymbols.diagramAccessor) private diagramAccessor: IDiagramAccessor,
        @inject(componentSymbols.buttonsForLayout) private buttonsForLayout: IButtonsForLayout,
        @inject(componentSymbols.customLayout) private customLayout: go.Layout) {
    }

    init() {
        this.diagram = this.diagramAccessor.getDiagram();
        this.diagram.layout = this.customLayout;
        this.addModelListener();

        this.buttonsForLayout.registerEventListeners(this.setAutoMode.bind(this), this.setManualMode.bind(this), this.refreshLayout.bind(this));
    }

    private setAutoMode() {
        this.diagram.layout.isOngoing = true;
        this.refreshLayout();
        this.addModelListener();
    }

    private setManualMode() {
        this.diagram.layout.isOngoing = false;
        this.removeModelListener();
    }

    private refreshLayout() {
        this.diagram.layoutDiagram(true);
    }

    private addModelListener() {
        this.diagram.addModelChangedListener(this.refreshLayoutOnTextChange);
    }

    private removeModelListener() {
        this.diagram.removeModelChangedListener(this.refreshLayoutOnTextChange);
    }

    private refreshLayoutOnTextChange = (event) => {
        if (event.propertyName === 'text') {
            this.refreshLayout();
        };
    }
}