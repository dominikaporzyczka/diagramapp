import { injectable, inject } from 'inversify';
import { componentSymbols } from '../IoC/Symbols';
import { IDiagramModels, IButtonsForDiagram } from '../interfaces/interfaces';

@injectable()
export class ButtonsForDiagram implements IButtonsForDiagram {
    constructor(@inject(componentSymbols.diagramModels) private diagramModels: IDiagramModels) {
    }

    registerClickListeners() {
        this.addListenerForSaveDiagramBtn();
        this.addlistenerForCreateDiagramBtn();
    }

    private addlistenerForCreateDiagramBtn() {
        const createDiagramBtn: HTMLElement = document.querySelector('.create-diagram-btn');

        createDiagramBtn.addEventListener('click', () => {
            this.diagramModels.addModel();
        });
    }

    private addListenerForSaveDiagramBtn() {
        const saveDiagramBtn: HTMLElement = document.querySelector('.save-diagram-btn');

        saveDiagramBtn.addEventListener('click', () => {
            this.diagramModels.saveModels();
        });
    }
}