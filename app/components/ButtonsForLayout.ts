import { IButtonsForLayout } from "../interfaces/interfaces";
import { injectable } from "inversify";

@injectable()
export class ButtonsForLayout implements IButtonsForLayout {
    private btnActiveClassName: string = 'active';

    registerEventListeners(callbackAutoMode: Function, callbackManualMode: Function, callbackOnKeyDown: Function) {
        this.addListenerForAutoModeBtn(callbackAutoMode, callbackOnKeyDown);
        this.addListenerforManualModeBtn(callbackManualMode, callbackOnKeyDown);
    }

    private addListenerForAutoModeBtn(callbackAutoMode: Function, callbackOnKeyDown: Function) {
        const autoModeBtn: HTMLButtonElement = document.querySelector('.layout-mode-btn--auto');

        autoModeBtn.addEventListener('click', () => {
            callbackAutoMode();
            this.setActiveClassName(autoModeBtn);
            this.removeListenerForKeyDown(callbackOnKeyDown);
        });
    }

    private addListenerforManualModeBtn(callbackManualMode: Function, callbackOnKeyDown: Function) {
        const manualModeBtn: HTMLButtonElement = document.querySelector('.layout-mode-btn--manual');

        manualModeBtn.addEventListener('click', () => {
            callbackManualMode();
            this.setActiveClassName(manualModeBtn);
            this.addListenerForKeyDown(callbackOnKeyDown);
        });
    }

    private addListenerForKeyDown(callbackOnKeyDown: Function) {
        document.addEventListener('keydown', this.onKeyDownHandler.bind(this, callbackOnKeyDown));
    }

    private removeListenerForKeyDown(callbackOnKeyDown: Function) {
        document.removeEventListener('keydown', this.onKeyDownHandler.bind(this, callbackOnKeyDown));
    }

    private onKeyDownHandler(callbackOnKeyDown: Function, event: KeyboardEvent) {
        const qCode = 81;

        if (event.ctrlKey && event.keyCode === qCode) {
            callbackOnKeyDown();
        }
    }

    private setActiveClassName(el: HTMLElement) {
        this.resetActiveClassNameFromBtns();
        el.classList.add(this.btnActiveClassName);
    }

    private resetActiveClassNameFromBtns() {
        const btn = document.querySelector(`.layout-mode-btn.${this.btnActiveClassName}`);
        if (btn) {
            btn.classList.remove(this.btnActiveClassName);
        }
    }
}
