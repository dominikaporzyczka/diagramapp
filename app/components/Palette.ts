import * as go from 'gojs';
import { injectable, inject, multiInject } from 'inversify';
import { constantsSymbols, componentSymbols } from '../IoC/Symbols';
import { IPaletteNodeTemplateProvider } from '../interfaces/interfaces';

@injectable()
export class Palette extends go.Palette {
    constructor( @inject(constantsSymbols.paletteSelector) paletteSelector: string,
        @multiInject(componentSymbols.paletteNodeTemplateProvider) nodeTemplateProvider: IPaletteNodeTemplateProvider[]) {
        super(document.querySelector(paletteSelector) as HTMLDivElement);

        nodeTemplateProvider.forEach(templateProvider => {
            const template = templateProvider.provideTemplate();
            this.nodeTemplateMap.add(template.category, template.template);
        });

        this.model.nodeDataArray = [{ key: 0, category: 'crescent', text: 'Moon' }, { key: 1, category: 'star', text: 'Star' }];
    }
}
