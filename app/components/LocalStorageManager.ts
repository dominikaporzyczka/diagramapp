import { injectable } from "inversify";
import { ILocalStorageManager } from "../interfaces/interfaces";

@injectable()
export class LocalStorageManager implements ILocalStorageManager {
    getInitalDiagramModels(key: string) {
        return JSON.parse(localStorage.getItem(key));
    }

    setToLocalStorage(key: string, value: string[]) {
        const convertedValue: string = JSON.stringify(value);
        localStorage.setItem(key, convertedValue);
    }
}