import { inject, injectable } from "inversify";
import { componentSymbols } from "../IoC/Symbols";
import { IDiagramModels, IButtonsForDiagram, IDiagramLayoutManager } from '../interfaces/interfaces';

@injectable()
export class InitApp {
    constructor( @inject(componentSymbols.diagramFactory) private diagramFactory: Function,
        @inject(componentSymbols.paletteFactory) private paletteFactory: Function,
        @inject(componentSymbols.diagramModels) private diagramModels: IDiagramModels,
        @inject(componentSymbols.diagramLayoutManager) private diagramLayoutManager: IDiagramLayoutManager,
        @inject(componentSymbols.buttonsForDiagram) private buttonsForDiagram: IButtonsForDiagram) {
    }

    init = () => {
        (window as any).myDiagram = this.diagramFactory();
        (window as any).myPalette = this.paletteFactory();

        this.diagramModels.init();
        this.diagramLayoutManager.init();
        this.buttonsForDiagram.registerClickListeners();
    }
}