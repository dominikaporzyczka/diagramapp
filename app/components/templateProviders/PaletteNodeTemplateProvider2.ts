import * as go from 'gojs';
import { injectable } from 'inversify';
import { IPaletteNodeTemplateProvider } from '../../interfaces/interfaces';

@injectable()
export class PaletteNodeTemplateProvider2 implements IPaletteNodeTemplateProvider {

    provideTemplate() {
        const category = 'star';
        const template = this.createTemplate();
        return {
            category,
            template
        }
    }

    private createTemplate() {
        const $ = go.GraphObject.make;

        return $(go.Node,
            go.Panel.Spot,
            $(go.Shape, 'FivePointedStar', { desiredSize: new go.Size(100, 100), fill: '#f1c40f', stroke: null })
        );
    }
}