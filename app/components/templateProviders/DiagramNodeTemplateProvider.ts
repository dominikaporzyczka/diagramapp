import * as go from 'gojs';
import { injectable } from 'inversify';
import { IDiagramNodeTemplateProvider } from '../../interfaces/interfaces';

@injectable()
export class DiagramNodeTemplateProvider implements IDiagramNodeTemplateProvider {

    provideTemplates() {
        const $ = go.GraphObject.make;

        return [
            {
                category: 'crescent',
                template:
                    $(go.Node,
                        go.Panel.Spot,
                        new go.Binding("text", "text"),
                        $(go.Shape, 'Crescent', { desiredSize: new go.Size(100, 100), fill: '#f53b57', stroke: null }),
                        $(go.TextBlock,
                            {
                                editable: true,
                                stroke: '#2c3e50',
                                alignment: go.Spot.Bottom
                            },
                            new go.Binding('text', 'text').makeTwoWay()
                        )
                    )
            },

            {
                category: 'star',
                template:
                    $(go.Node,
                        go.Panel.Spot,
                        new go.Binding("text", "text"),
                        $(go.Shape, 'FivePointedStar',
                            {
                                desiredSize: new go.Size(100, 100),
                                fill: '#f1c40f',
                                stroke: null
                            }
                        ),
                        $(go.TextBlock,
                            {
                                editable: true,
                                stroke: '#2c3e50',
                                alignment: go.Spot.Bottom
                            },
                            new go.Binding('text', 'text').makeTwoWay()
                        )
                    )
            }
        ]
    }
}