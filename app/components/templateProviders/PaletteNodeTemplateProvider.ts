import * as go from 'gojs';
import { injectable } from 'inversify';
import { IPaletteNodeTemplateProvider } from '../../interfaces/interfaces';

@injectable()
export class PaletteNodeTemplateProvider implements IPaletteNodeTemplateProvider {

    provideTemplate() {
        const category = 'crescent';
        const template = this.createTemplate();
        return {
            category,
            template
        }
    }

    private createTemplate() {
        const $ = go.GraphObject.make;

        return $(go.Node,
            go.Panel.Spot,
            $(go.Shape, 'Crescent', { desiredSize: new go.Size(100, 100), fill: '#f53b57', stroke: null })
        );
    }
}